cmake_minimum_required(VERSION 3.0)
add_compile_options(-std=c++11)

project(icp)

add_library(${PROJECT_NAME} icp.cpp)

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
install(TARGETS ${PROJECT_NAME} DESTINATION lib)
install(FILES icp.h DESTINATION include)
install(DIRECTORY Eigen DESTINATION include)
